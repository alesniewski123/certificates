# My Certificates
## IBM Data Science Professional Certificate
https://www.coursera.org/professional-certificates/ibm-data-science

<img src="img/img_data_science.png" height="400">

https://www.coursera.org/account/accomplishments/professional-cert/N66ZYZ6G3L75

Earned after completing each course in the Specialization

#### Tools for Data Science
IBM

Taught by: Aije Egwaikhide, Svetlana Levitan & Romeo Kienzler

Completed by: ADAM LEŚNIEWSKI by October 10, 2021

3 weeks of study, 2-3 hours/week

https://www.coursera.org/account/accomplishments/certificate/VA3TM34YGJ8L

#### Python Project for Data Science
IBM

Taught by: Azim Hirjani & Joseph Santarcangelo

Completed by: ADAM LEŚNIEWSKI by October 10, 2021

https://www.coursera.org/account/accomplishments/certificate/T5BHWFY9R6JJ

#### Machine Learning with Python
IBM

Taught by: SAEED AGHABOZORGI & Joseph Santarcangelo

Completed by: ADAM LEŚNIEWSKI by October 10, 2021

5-6 weeks of study, 3-6 hours per week

https://www.coursera.org/account/accomplishments/certificate/MX4LURMJX8U7

#### Data Analysis with Python
IBM

Taught by: Joseph Santarcangelo

Completed by: ADAM LEŚNIEWSKI by October 10, 2021

This course requires approximately two hours a week for six weeks.

https://www.coursera.org/account/accomplishments/certificate/DGPC6UTRD4MW

#### What is Data Science?
IBM

Taught by: Rav Ahuja & Alex Aklson

Completed by: ADAM LEŚNIEWSKI by July 13, 2021

3 weeks of study, 2-3 hours/week

https://www.coursera.org/account/accomplishments/certificate/YEKU7GGE23FK

#### Databases and SQL for Data Science with Python
IBM

Taught by: Rav Ahuja & Hima Vasudevan

Completed by: ADAM LEŚNIEWSKI by October 2, 2021

4 weeks of study, 2-4 hours/week.

https://www.coursera.org/account/accomplishments/certificate/APCUY5RJG9BM

#### Data Visualization with Python
IBM

Taught by: Saishruthi Swaminathan

Completed by: ADAM LEŚNIEWSKI by October 10, 2021

3 weeks of study, 4-5 hours/week

https://www.coursera.org/account/accomplishments/certificate/E35HFWJYMTKT

#### Python for Data Science, AI & Development
IBM

Taught by: Joseph Santarcangelo

Completed by: ADAM LEŚNIEWSKI by July 29, 2021

https://www.coursera.org/account/accomplishments/certificate/3BQYVSALS428

#### Applied Data Science Capstone
IBM

Taught by: Yan Luo & Joseph Santarcangelo

Completed by: ADAM LEŚNIEWSKI by October 10, 2021

5 weeks of study, approximately 45 hours in total (10-15 hours/week for the later modules)

https://www.coursera.org/account/accomplishments/certificate/CKFGH4XS78NG

#### Data Science Methodology
IBM

Taught by: Alex Aklson & Polong Lin

Completed by: ADAM LEŚNIEWSKI by October 10, 2021

3 weeks of study, 2 - 3 hours/week

https://www.coursera.org/account/accomplishments/certificate/SBZD6DGBUDEB
